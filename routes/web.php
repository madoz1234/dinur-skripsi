<?php

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/home', function(){
  return redirect()->route('dashboard.home.index');
});

Route::middleware('auth')->group(function() {
	// Route::name('dashboard.')->prefix('dashboard')->namespace('Dashboard')->group( function() {
	// 	Route::get('/', function(){
	// 		return redirect()->route('dashboard.home.index');
	// 	});
	// 	Route::resource('home', 'HomeController');
	// });

	Route::name('master.')->prefix('master')->namespace('Master')->group( function() {
        Route::post('kriteria/grid', 'KriteriaController@grid')->name('kriteria.grid');
		Route::get('kriteria/detil/{id}','KriteriaController@detail')->name('kriteria.detail');
		Route::patch('kriteria/{id}/simpan','KriteriaController@simpan')->name('kriteria.simpan');
		Route::resource('kriteria', 'KriteriaController');

		Route::post('kedekatan/grid', 'KedekatanController@grid')->name('kedekatan.grid');
		Route::get('kedekatan/detil/{id}','KedekatanController@detail')->name('kedekatan.detail');
		Route::patch('kedekatan/{id}/simpan','KedekatanController@simpan')->name('kedekatan.simpan');
		Route::resource('kedekatan', 'KedekatanController');

		Route::post('sample/grid', 'SampleController@grid')->name('sample.grid');
		Route::get('sample/detil/{id}','SampleController@detail')->name('sample.detail');
		Route::patch('sample/{id}/simpan','SampleController@simpan')->name('sample.simpan');
		Route::resource('sample', 'SampleController');

		Route::post('kasus/grid', 'KasusController@grid')->name('kasus.grid');
		Route::get('kasus/detil/{id}','KasusController@detail')->name('kasus.detail');
		Route::get('kasus/status/{id}','KasusController@status')->name('kasus.status');
		Route::resource('kasus', 'KasusController');
	});

	Route::name('setting.')->prefix('setting')->namespace('Setting')->group( function() {
		Route::get('/', function(){
			return redirect()->route('setting.users.index');
		});

		Route::post('users/grid', 'UserController@grid')->name('users.grid');
		Route::get('users/{id}/detail','UserController@detail')->name('users.detail');
		Route::resource('users', 'UserController');

		Route::post('log/grid', 'LogController@grid')->name('log.grid');
		Route::resource('log', 'LogController');

		Route::post('roles/grid', 'RoleController@grid')->name('roles.grid');
		Route::patch('roles/saveData/{id}','RoleController@saveData')->name('roles.saveData');
		Route::resource('roles', 'RoleController');
	});
});

Auth::routes();
