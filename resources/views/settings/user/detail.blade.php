<form action="" id="formData">
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Detil Data User</h5>
    </div>
    <div class="modal-body">
        <table class="table table-bordered" style="font-size: 12px;">
			<tbody>
				<tr>
					<td style="width: 110px;">NIP</td>
					<td style="width: 10px;">:</td>
					<td style="text-align: left;">
						{{$record->nip}}
					</td>
				</tr>
				<tr>
					<td>Nama</td>
					<td>:</td>
					<td style="text-align: left;">
						{{$record->name}}
					</td>
				</tr>
				<tr>
					<td>Tanggal Lahir</td>
					<td>:</td>
					<td style="text-align: left;">
						{{DateToString($record->tgl_lahir)}}
					</td>
				</tr>
				<tr>
					<td>Jenis Kelamin</td>
					<td>:</td>
					<td style="text-align: left;">
						@if($record->jk == 0)
						 Laki - Laki
						@else 
						 Perempuan
						@endif
					</td>
				</tr>
				<tr>
					<td>Email</td>
					<td>:</td>
					<td style="text-align: left;">
						{{$record->email}}
					</td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td>:</td>
					<td style="text-align: left;text-justify: inter-word;">
						{!! $record->readMoreText('alamat', 150) !!}
					</td>
				</tr>
				<tr>
					<td>Status</td>
					<td>:</td>
					<td style="text-align: left;">
						@if($record->status == 0)
							<span class="label label-success">Aktif</span>
						@else 
							<span class="label label-danger">Tidak Aktif</span>
						@endif
					</td>
				</tr>
			</tbody>
		</table>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>