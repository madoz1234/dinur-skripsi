<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>EKIPA - ONLINE CERTIFICATION</title>
        <link rel="shortcut icon" type="image/png" href="{{ asset('favicon.png') }}"/>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('gate/css/bootstrap.min.css') }}">
        <!-- Fonts -->
        <link rel="stylesheet" type="text/css" href="{{ asset('gate/fonts/line-icons.css') }}">
        <!-- Slicknav -->
        <link rel="stylesheet" type="text/css" href="{{ asset('gate/css/slicknav.css') }}">
        <!-- Off Canvas Menu -->
        <link rel="stylesheet" type="text/css" href="{{ asset('gate/css/menu_sideslide.css') }}">
        <!-- Color Switcher -->
        <link rel="stylesheet" type="text/css" href="{{ asset('gate/css/vegas.min.css') }}">
        <!-- Animate -->
        <link rel="stylesheet" type="text/css" href="{{ asset('gate/css/animate.css') }}">
        <!-- Main Style -->
        <link rel="stylesheet" type="text/css" href="{{ asset('gate/css/main.css') }}">
        <!-- Responsive Style -->
        <link rel="stylesheet" type="text/css" href="{{ asset('gate/css/responsive.css') }}">
    </head>
    <body>
        <div class="bg-wraper overlay has-vignette">
            <div id="example" class="slider opacity-50 vegas-container" style="height: 983px;"></div>
        </div>
        <!-- Coundown Section Start -->
        <section class="countdown-timer">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <img src="{{ asset('gate/img/logo.png') }}" class="heading-img" alt="ekipa" width="300px">
                        <div class="heading-count">
                            <h3 style="font-weight: thin !important;">Online Certification</h3>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        {{-- <div class="row time-countdown justify-content-center">
                            <div id="clock" class="time-count"></div>
                        </div> --}}
                        <p class="sr-only">
                            Lovie - Lorem ipsum dolor sit amet, consectetur adipisicing elit. At quis obcaecati voluptates. Cupiditate sapiente eaque beatae asperiores assumenda dolore nemo perspiciatis rerum quisquam ea similique, accusamus id suscipit veniam et.
                        </p>

                        @if (Route::has('login'))
                            <div class="button-group mt-1">
                                @auth
                                    <a href="{{ url('/home') }}" class="btn btn-common">Home</a>
                                @else
                                    <a href="{{ route('login') }}" class="btn btn-common">Login</a>

                                    @if (Route::has('register'))
                                        <a href="{{ route('register') }}" class="btn btn-border">Register</a>
                                    @endif
                                @endauth
                            </div>
                        @endif
                        <div class="social mt-4">
                            <a class="facebook" href="#"><i class="lni-facebook-filled"></i></a>
                            <a class="twitter" href="#"><i class="lni-twitter-filled"></i></a>
                            <a class="instagram" href="#"><i class="lni-instagram-filled"></i></a>
                            <a class="google" href="#"><i class="lni-google-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Coundown Section End -->
        <!-- Preloader -->
        <div id="preloader">
            <div class="loader" id="loader-1"></div>
        </div>
        <!-- End Preloader -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ asset('gate/js/jquery-min.js') }}"></script>
        <script src="{{ asset('gate/js/popper.min.js') }}"></script>
        <script src="{{ asset('gate/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('gate/js/vegas.min.js') }}"></script>
        <script src="{{ asset('gate/js/jquery.countdown.min.js') }}"></script>
        <script src="{{ asset('gate/js/classie.js') }}"></script>
        <script src="{{ asset('gate/js/jquery.nav.js') }}"></script>
        <script src="{{ asset('gate/js/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('gate/js/wow.js') }}"></script>
        <script src="{{ asset('gate/js/jquery.slicknav.js') }}"></script>
        <script src="{{ asset('gate/js/main.js') }}"></script>
        <script src="{{ asset('gate/js/form-validator.min.js') }}"></script>
        <script src="{{ asset('gate/js/contact-form-script.min.js') }}"></script>
        <script type="text/javascript">
            // $("#example").vegas({
            //     timer: false,
            //     delay: 6000,
            //     transitionDuration: 2000,
            //     transition: "blur",
            //     overlay: true,
            //     cover: true,
            //     slides: [
            //         { src: "{{ asset('gate/img/bg1.jpg') }}" },
            //     ]
            // });
        </script>
    </body>
</html>
