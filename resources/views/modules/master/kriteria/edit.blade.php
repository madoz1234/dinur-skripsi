<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data Kriteria</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group field">
            <label class="control-label">Nama Kriteria</label>
            <input type="text" name="nama" class="form-control" placeholder="Nama Kriteria" value="{{ $record->nama }}">
        </div>
        <div class="form-group field">
            <label class="control-label">Bobot Kriteria</label>
            <input type="number" name="bobot" class="form-control" placeholder="Bobot Kriteria" value="{{ $record->bobot }}">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>