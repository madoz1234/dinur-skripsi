@extends('layouts.base')

@push('css')
<link rel="stylesheet" href="#" type="text/css" />
<link rel="stylesheet" href="{{ asset('libs/assets/one-calender/simple-calendar.css') }}" type="text/css" />
@endpush

@push('js')
<script src="{{ asset('libs/assets/one-calender/jquery.simple-calendar.js') }}"></script>
<script src="http://malsup.github.com/jquery.form.js"></script> 
{{-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://apexcharts.com/samples/assets/stock-prices.js') }}"></script> --}}
@endpush

@section('title', 'Detil Kasus')

@section('side-header')
<div style="margin-right: 14px;">
  <span><i class="glyphicon glyphicon-home"></i></span>
</div>
@endsection

@push('styles')
@endpush

@push('scripts')
@endpush

@section('body')
<div class="row row-sm">
  <div class="panel b-a">
    <div class="panel-body" style="padding-bottom: 0px;">
      <form action="" method="POST" id="formData">
        @method('PATCH')
        @csrf
        <input type="hidden" name="id" value="{{ $record->id }}">
        <div class="loading dimmer padder-v" style="display: none;">
            <div class="loader"></div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                    <tbody class="container">
                        <tr>
                            <td style="text-align: center;width: 50px;">{{ $record->nama }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                    <tbody class="container">
                        <tr>
                            <td style="text-align: center;width: 50px;" rowspan="2">Kriteria</td>
                            @foreach($record->detail as $a)
                            <td style="text-align: center;width: 50px;">{{ $a->kriteria->nama }}</td>
                            @endforeach
                        </tr>
                        <tr>
                            @foreach($record->detail as $a)
                            <td style="text-align: center;width: 50px;">{{ $a->detail_kriteria->nama }}</td>
                            @endforeach
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-12">
                <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                    <tbody class="container">
                        <tr>
                            <td style="text-align: center;width: 50px;">Kriteria</td>
                            @foreach($kriteria as $a)
                            <td style="text-align: center;width: 50px;">{{ $a->nama }}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td style="text-align: center;width: 50px;">Bobot</td>
                            @foreach($kriteria as $a)
                            <td style="text-align: center;width: 50px;">{{ $a->bobot }}</td>
                            @endforeach
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-12">
                <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                    <tbody class="container">
                        <tr>
                            <td style="text-align: center;width: 50px;">Nilai Kedekatan</td>
                            @foreach($record->detail as $a)
                            <td style="text-align: center;width: 50px;">{{ $a->kriteria->nama }}</td>
                            @endforeach
                        </tr>
                        @foreach($sample as $b)
                        <tr>
                            <td style="width: 50px;">{{ $b->nama }}</td>
                            @foreach($record->detail as $a)
                            @php
                                $cari = carikedekatan($a->kriteria_id, $b->detail->where('kriteria_id', $a->kriteria_id)->first()->detail_id, $a->detail_id);
                            @endphp
                            <td style="text-align: center;width: 50px;">{{ $cari->nilai }}</td>
                            @endforeach
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-12">
                <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                    <tbody class="container">
                        <tr>
                            <td style="text-align: center;width: 50px;">Nilai Kedekatan x Bobot</td>
                            @foreach($record->detail as $a)
                            <td style="text-align: center;width: 50px;">{{ $a->kriteria->nama }}</td>
                            @endforeach
                        </tr>
                        @php 
                            $sum =0;
                            $total=array();
                        @endphp
                        @foreach($sample as $b)
                        <tr>
                            <td style="width: 50px;">{{ $b->nama }}</td>
                            @foreach($record->detail as $a)
                            @php
                                $cari = carikedekatan($a->kriteria_id, $b->detail->where('kriteria_id', $a->kriteria_id)->first()->detail_id, $a->detail_id);
                                $bobot = $kriteria->where('id', $a->kriteria_id)->first();
                                $nilai = $cari->nilai * $bobot->bobot;
                                $sum += $nilai;
                            @endphp
                            <td style="text-align: center;width: 50px;">({{ $cari->nilai }} x {{ $bobot->bobot }}) = {{ $nilai }}</td>
                            @endforeach
                            @php
                                $total[$b->id]= $sum;
                                $sum =0;
                            @endphp
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                    <tbody class="container">
                        <tr>
                            <td style="text-align: center;width: 50px;">(Total Nilai Kedekatan / Total Bobot)</td>
                            <td style="text-align: center;width: 50px;">Hasil</td>
                        </tr>
                        @php 
                            $tot =0;
                            $totz=array();
                        @endphp
                        @foreach($sample as $b)
                        <tr>
                            @php 
                                $sumkriteria = $kriteria->sum('bobot');
                                $tot = ($total[$b->id] / $sumkriteria);
                            @endphp
                            <td style="width: 50px;">{{ $b->nama }}</td>
                            <td style="text-align: center;width: 50px;">{{ $total[$b->id] }} / {{ $sumkriteria }} = {{ round($tot, 3) }}</td>
                            @php
                                $totz[$b->id]= $tot;
                                $tot =0;
                            @endphp
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                    <tbody class="container">
                        <tr>
                            <td style="text-align: center;width: 50px;">Sampel</td>
                            <td style="text-align: center;width: 50px;">Hasil Akhir</td>
                            <td style="text-align: center;width: 50px;">Status</td>
                        </tr>
                        @php
                        arsort($totz);
                        @endphp
                        @foreach($totz as $key => $a)
                        @php
                            $data = $sample->where('id', $key)->first();
                        @endphp
                        <tr>
                            <td style="width: 50px;">{{ $data->nama }}</td>
                            <td style="text-align: center;width: 50px;">{{ round($totz[$data->id], 3) }}</td>
                            <td style="text-align: center;width: 50px;">
                                @if($data->status == 0) 
                                    <span class="label label-danger">Tidak Layak</span> 
                                @else 
                                    <span class="label label-success">Layak</span> 
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @php
            $maxVal = max($totz);
            $maxKey = array_search($maxVal, $totz);
            $datas = $sample->where('id', $maxKey)->first();
        @endphp
        <div class="form-row">
            <p style="text-align: justify;text-justify: inter-word;">Berdasarkan perhitungan K-Nearest Neighbor dengan nama kasus <span style="color:blue;font-weight: bold;">{{ $record->nama }}</span> didapat hasil nilai kedekatan tertinggi yaitu dengan nama sampel <span style="color:blue;font-weight: bold;">{{ $datas->nama }}</span> dengan nilai kedekatan akhir <span style="color:blue;font-weight: bold;">{{ round($maxVal, 3) }}</span> <br> Dari hasil kedekatan dengan sampel <span style="color:blue;font-weight: bold;">{{ $datas->nama }}</span> dapat disimpulkan bahwa kasus <span style="color:blue;font-weight: bold;">{{ $record->nama }}</span> mendapatkan status : @if($datas->status == 0) <span class="label label-danger">Tidak Layak</span> @else <span class="label label-success">Layak</span> @endif </p>
            @php
                updatekasus($record->id, $datas->status);
            @endphp
        </div>
        <p>&nbsp;</p>
        <div class="form-row">
          <div class="form-group col-md-12">
            <div class="text-left">
              <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script>
</script>
@yield('js-extra')
@endpush