<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data Kasus</h5>
    </div>
    <div class="modal-body">
        <div class="form-group field">
            <label class="control-label">Nama Kasus</label>
            <input type="text" name="nama" class="form-control" placeholder="Nama Kasus" required="">
        </div>
        <div class="form-group field">
            <label class="control-label">Lengkapi Kriteria</label>
        </div>
        @foreach($record as $key => $kriteria)
            <div class="form-group field">
                <label class="control-label">{{ $kriteria->nama }}</label>
                <select class="selectpicker form-control" name="detail[{{ $kriteria->id }}][kasus]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                    @foreach($detail->where('kriteria_id', $kriteria->id) as $datas)
                    <option value="{{ $datas->id }}">{{ $datas->nama }}</option>
                    @endforeach
                </select>  
            </div>
        @endforeach
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>