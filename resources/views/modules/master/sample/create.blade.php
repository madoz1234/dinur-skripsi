<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data Sample</h5>
    </div>
    <div class="modal-body">
        <div class="form-group field">
            <label class="control-label">Nama Sample</label>
            <input type="text" name="nama" class="form-control" placeholder="Nama Sample" required="">
        </div>
        <div class="form-group field">
            <label class="control-label">Status</label><br>
            <input type="checkbox" name="status" data-width="100" data-toggle="toggle" data-size="mini" checked data-on="Layak" data-off="Tidak Layak" data-style="ios">            
        </div>
        <div class="form-group field">
            <label class="control-label">Lengkapi Kriteria</label>
        </div>
        @foreach($record as $key => $kriteria)
            <div class="form-group field">
                <label class="control-label">{{ $kriteria->nama }}</label>
                <select class="selectpicker form-control" name="detail[{{ $kriteria->id }}][sample]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                    @foreach($detail->where('kriteria_id', $kriteria->id) as $datas)
                    <option value="{{ $datas->id }}">{{ $datas->nama }}</option>
                    @endforeach
                </select>  
            </div>
        @endforeach
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>