<script type="text/javascript">
	$(document).ready(function() {
		app.init();
var options = {
          series: [{
          name: 'Aktif',
          data: [{{$aktif}}]
        }, {
          name: 'Tidak Aktif',
          data: [{{$naktif}}]
        }],
          chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        },
        yaxis: {
          title: {
            text: 'Tugas'
          }
        },
        fill: {
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val + " Tugas"
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#charts"), options);
        chart.render();
});
</script>
<div id="charts"></div>
