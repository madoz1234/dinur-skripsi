@extends('layouts.base')

@push('css')
    <link rel="stylesheet" href="{{ asset('libs/assets/one-calender/simple-calendar.css') }}" type="text/css" />
@endpush

@push('js')
    <script src="{{ asset('libs/assets/one-calender/jquery.simple-calendar.js') }}"></script>
@endpush

@section('title', 'Dashboard')

@section('side-header')
<div style="margin-right: 14px;">
	<span><i class="glyphicon glyphicon-home"></i></span>
</div>
@endsection

@push('styles')
<style>
	.nav-pills > li.active > a,
	.nav-pills > li.active > a:hover,
	.nav-pills > li.active > a:focus {
	  color: #fff;
	  background-color: #448BFF;
	}
	.nav-pills > li > a {
	  border-radius: 0px;
	}
</style>
@endpush
  <?php
    $test_array = [];
    if(isset($user)){
      foreach($user as $key => $row){
          $tmp = [
            'lat' => $row->lat,
            'lng' => $row->lng,
            'nama' => $row->employe->name,
          ];
          $test_array[] = $tmp;
      }
    }
  ?>
@push('scripts')
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFlm1JrVjXexh-QSlQJYVcP_4dKSNVCJU&callback=initMap&libraries=geometry">
</script>
<script type="text/javascript">
	$(document).ready(function() {
		var tahun_g = $('input[name=tahun_g]').val();
		reloadDataGrafik(tahun_g);
		reloadDataGrafikEmploye();
		var name = <?php echo json_encode($jadwal); ?>;
		var text =[];
		for (i = 0; i < name.length; i++) {
		  text.push({
			      startDate: name[i]['startDate'],
			      endDate: name[i]['startDate'],
			      summary: name[i]['summary']
			    },);
		}
		$("#calender").simpleCalendar({
	      fixedStartDay: false,
	      disableEmptyDetails: true,
	      events: text,
	    });
	});
	function initMap() {
        var new_marker;
        var markers = [];
        var icon_center ={
          url: '{{ asset('src/img/usr.png') }}',
          scaledSize: new google.maps.Size(30, 30),
        };
        // Create the map.
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: {lat: -6.1742526, lng: 106.7862555},
          // mapTypeId: 'Map'
        });

        var data = <?php echo json_encode($test_array); ?>;
		$.each(data, function(d,i){
	        var post;
	        var post = new google.maps.LatLng(parseFloat(i.lat), parseFloat(i.lng));
	        var new_marker = new google.maps.Marker({
	            position: post,
	            map: map,
	            icon:icon_center,
	            title: i.nama,
	        });
	        markers.push(new_marker);
	        
	        var circle = new google.maps.Circle({
	          map: map,
	          radius: 10000,    // 10 miles in metres
	          fillColor: "#4e4efc",//'#fc4e59',
	          strokeColor: "#4e4efc",
	          strokeOpacity: 0.8,
	          strokeWeight: 0,
	          fillOpacity: 0.4,
	        });
	        circle.bindTo('center', new_marker, 'position');
        });
    };

    var app = {
		settings: {
			container: $('.calendar'),
			calendar: $('.front'),
			days: $('.weeks span'),
			form: $('.back'),
			input: $('.back input'),
			buttons: $('.back button')
		},

		init: function() {
			instance = this;
			settings = this.settings;
			this.bindUIActions();
		},

		swap: function(currentSide, desiredSide) {
			settings.container.toggleClass('flip');

			currentSide.fadeOut(900);
			currentSide.hide();

			desiredSide.show();
		},

		bindUIActions: function() {
			settings.days.on('click', function(){
				instance.swap(settings.calendar, settings.form);
				settings.input.focus();
			});

			settings.buttons.on('click', function(){
				instance.swap(settings.form, settings.calendar);
			});
		}
	}

    $('.tahun_g').datepicker({
        format: "yyyy",
	    viewMode: "years",
	    minViewMode: "years",
        orientation: "auto",
        autoclose:true
    }).on('changeDate', function (selected) {
    	var mins = new Date(selected.date.valueOf());
	    reloadDataGrafik(mins.getFullYear())
	});

	var reloadDataGrafik = function(tahun){
        $.ajax({
			url: '{!! route($routes.'.chartTask') !!}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				tahun: tahun,
			},
		}).done(function(responses) {
			$('#thn-g').html(responses)
		}).fail(function() {
			console.log("error");
		});
    }

    var reloadDataGrafikEmploye = function(tahun){
        $.ajax({
			url: '{!! route($routes.'.chartEmploye') !!}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
			},
		}).done(function(responses) {
			$('#employe-g').html(responses)
		}).fail(function() {
			console.log("error");
		});
    }
</script>
@endpush

@section('body')
	<div class="row row-sm">
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="panel panel-default">
				<div class="panel-heading font-bold">Jadwal</div>
				<div class="panel-body">
					<div id="calender" class="calendar-container" style="padding: 10px;"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="panel panel-default">
				<div class="panel-heading font-bold">Grafik Employe</div>
				<div class="panel-body">
					<div class="panel-body">
						<div id="employe-g"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row row-sm">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading font-bold">Grafik Task</div>
				<div class="panel-body">
					<form id="dataFilters" class="form-inline" role="form">
						<div class="form-group">
							<label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
							<input type="text" class="form-control tahun_g" name="tahun_g" id="tahun_g" data-toggle="months" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
						</div>
					</form>
					<div class="panel-body">
						<div id="thn-g"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row row-sm">
		<div class="col-lg-12 col-md-3 col-sm-3">
			<div class="row" style="margin: 0px 0px 20px 0px">
					<div class="col-sm-12 text-right">
						<div id="map" style="height: 700px"></div>
					</div>
			</div>
		</div>
	</div>
@endsection
