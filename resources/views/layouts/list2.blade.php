@extends('layouts.base')

@include('libs.datatable')
@include('libs.actions')
@section('side-header')
@endsection
@section('body')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-8" style="left: 17px;">
                    <form id="dataFilters" class="form-inline" role="form">
                        @yield('filters')
                    </form>
                </div>
                <div class="col-sm-4 text-right">
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding-top: 0px;padding-bottom: 0px;">
            <div class="table-responsive">
                @if(isset($tableStruct))
                    <table id="dataTable" class="table table-bordered m-t-none">
                        <thead>
                            <tr>
                                @foreach ($tableStruct as $struct)
                                    <th class="text-center v-middle">{{ $struct['label'] }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @yield('tableBody')
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var modal = '#mediumModal';
        var onShow = function(){
    	   $('.tahun').datepicker({
                format: "yyyy",
			    viewMode: "years",
			    minViewMode: "years",
                orientation: "auto",
                autoclose:true
           });
           $('.tgl').datepicker({
                format: "yyyy-mm-dd",
                orientation: "auto",
                autoclose:true
           });
           $("#xls").fileinput({
             language: 'es',
             uploadUrl: "/file-upload-batch/2",
             previewFileType: "xlsx",
             showUpload: false,
             previewFileIcon: '<i class="fas fa-file"></i>',
             previewFileIconSettings: {
                 'docx': '<i class="fas fa-file-word text-primary"></i>',
                 'xlsx': '<i class="fas fa-file-excel text-success"></i>',
                 'pptx': '<i class="fas fa-file-powerpoint text-danger"></i>',
                 'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
                 'zip': '<i class="fas fa-file-archive text-muted"></i>',
             },
             fileActionSettings: {
               showRemove: true,
               showUpload: false, //This remove the upload button
               showZoom: true,
               showDrag: true
           },
             initialPreviewAsData: true,
             purifyHtml: true,
             allowedFileExtensions: ['xlsx', 'xls'],
               // allowed image size up to 5 MB
               maxFileSize: 5000,
             previewFileExtSettings: {
               'doc': function(ext) {
                 return ext.match(/(doc|docx)$/i);
               },
               'xls': function(ext) {
                 return ext.match(/(xls|xlsx)$/i);
               },
               'ppt': function(ext) {
                 return ext.match(/(ppt|pptx)$/i);
               }
             }
         	});

           $('#gambar').fileinput({
			    language: 'es',
			    uploadUrl: "/file-upload-batch/2",
			    previewFileType: "image",
			    showUpload: false,
			    previewFileIcon: '<i class="fas fa-file"></i>',
			    fileActionSettings: {
				    showRemove: true,
				    showUpload: false, //This remove the upload button
				    showZoom: true,
				    showDrag: true
				},
			    initialPreviewAsData: true,
			    purifyHtml: true,
			});

    	   $('.bulan-tahun').datepicker({
                format: "mm-yyyy",
			    viewMode: "months",
			    minViewMode: "months",
                orientation: "auto",
                autoclose:true
           });

            $(".tlp").on("keyup", function(e){
				setTimeout(jQuery.proxy(function() {
			        this.val(this.val().replace(/[^0-9]/g, ''));
			    }, $(this)), 0);
			});
			$("[data-toggle='toggle']").bootstrapToggle('destroy')
		    $("[data-toggle='toggle']").bootstrapToggle();
        };
        $(document).on('click', '.add.button', function(e){
            loadModal({
                url: "{!! route($routes.'.create') !!}",
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });
        $(document).on('click', '.custom.button', function(e){
						var url = $(this).data('url');
            loadModal({
                url: url,
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });

	    $(document).on('click', '.hapus_deskripsi', function (e){
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#table-data');
			var rows = table.find('tbody tr');
		});

        $(document).on('click', '.edit.button', function(e){
            var idx = $(this).data('id');

            loadModal({
                url: "{!! route($routes.'.index') !!}/" + idx + '/edit',
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });

        $(document).on('click', '.save.button', function(e){
            saveData('formData', function(resp){
                $(modal).modal('hide');
                dt.draw(false);
            });
        });

        $(document).on('click', '.delete.button', function(e){
            var idx = $(this).data('id');

            deleteData('{!! route($routes.'.index') !!}/' + idx, function(resp){
                dt.draw(false);
            });
        });

        $(document).on('submit', '#formData', function(e){
            e.preventDefault();

            $('.save.button').trigger('click');
        });

        $(document).on('click', '.detil.button', function(e){
            var idx = $(this).data('id');
            var url = "{!! route($routes.'.index') !!}/detil/" + idx;
            window.location = url;
        });

        $(document).on('click', '.lihat.button', function(e){
            var idx = $(this).data('id');
            var url = "{!! route($routes.'.index') !!}/" + idx;
            window.location = url;
        });

        $(document).on('click', '.detail.button', function(e){
        	var idx = $(this).data('id');
        	loadModal({
                url: "{!! route($routes.'.index') !!}/" + idx + '/detail',
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });
        $(document).on('click', '.details.button', function(e){
        	var idx = $(this).data('id');
        	loadModal({
                url: "{!! route($routes.'.index') !!}/" + idx + '/detail',
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });
    </script>

    @yield('js-extra')
@endpush
