<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-centers {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .code {
                border-right: 2px solid;
                font-size: 26px;
                padding: 0 15px 0 15px;
                text-align: center;
            }

            .codes {
                font-size: 26px;
                padding: 50px 15px 50px 15px;
                text-align: center;
            }

            .message {
                font-size: 18px;
                text-align: center;
            }

            .btn-cancel {
		        background-color:#DDDDDD;
		        color:#000;
		        text-shadow:none;
		        background-image:none;
		        -webkit-box-shadow: 0 1px 1px rgba(90, 90, 90, 0.1);
		        box-shadow: 0 1px 1px rgba(90, 90, 90, 0.1);
		        border-radius: 15px;
		        font-size: 12px;
		        font-weight:bold;
		        padding-top:5px;
		        padding-bottom:4px;
		        border-radius: 15px;
		    }

		    .btn-cancel:hover,
		    .btn-cancel:focus,
		    .btn-cancel:active,
		    .btn-cancel.active,
		    .open .dropdown-toggle.btn-cancel {
		        border-color: #BBBABA;
		        background-color:#27292a;
		        color:#fff;
		        text-shadow:none
		    }

		    .btn-cancel:active,
		    .btn-cancel.active,
		    .open .dropdown-toggle.btn-cancel {
		        background-image: none;
		    }

		    .btn-cancel.disabled,
		    .btn-cancel[disabled],
		    fieldset[disabled] .btn-cancel,
		    .btn-cancel.disabled:hover,
		    .btn-cancel[disabled]:hover,
		    fieldset[disabled] .btn-cancel:hover,
		    .btn-cancel.disabled:focus,
		    .btn-cancel[disabled]:focus,
		    fieldset[disabled] .btn-cancel:focus,
		    .btn-cancel.disabled:active,
		    .btn-cancel[disabled]:active,
		    fieldset[disabled] .btn-cancel:active,
		    .btn-cancel.disabled.active,
		    .btn-cancel[disabled].active,
		    fieldset[disabled] .btn-cancel.active {
		        background-color: #fcfdfd;
		        border-color: #dee5e7;
		    }

		    .btn-cancel.btn-bg {
		        border-color: rgba(0, 0, 0, 0.1);
		        background-clip: padding-box;
		    }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="code">
                @yield('code')
            </div>

            <div class="message" style="padding: 10px;">
                @yield('message')
            </div>
        </div>
        @yield('button')

        {{-- <div class="">
            <div class="codes">
               <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
            </div> --}}
        </div>
    </body>
</html>
