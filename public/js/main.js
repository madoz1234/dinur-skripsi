let token = document.head.querySelector('meta[name="csrf-token"]').content;

$(document).on('click', '.logout', function(e){
	$.redirect('/logout', {'_token': token});
});

$(document).on('input, keydown', 'textarea.inline', function(e){
    var el = this;
    setTimeout(function(){
        el.style.cssText = 'height:20px; padding:0';
        // for box-sizing other than "content-box" use:
        el.style.cssText = '-moz-box-sizing:content-box';
        el.style.cssText = 'height:' + el.scrollHeight + 'px';
    },0);
});