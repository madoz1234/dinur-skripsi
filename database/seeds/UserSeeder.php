<?php

use Illuminate\Database\Seeder;

use App\Models\Auths\User;
use Spatie\Permission\Models\Role;


class UserSeeder extends Seeder
{
    public function run()
    {

    	// create user
		$user = [
			[
				'nip'  => '000001',
				'name'  => 'Super Admin',
				'email' => 'admin@email.com',
				'tgl_lahir' => '06-05-1995',
				'password' => bcrypt('password'),
				'perms' => 'admin',
			],
			[
				'nip'  => '000002',
				'name'  => 'Ahmadi Surahman',
				'email' => 'ahmadi@email.com',
				'tgl_lahir' => '06-05-1995',
				'password' => bcrypt('password'),
				'perms' => 'employe',
			]
		];
		
		foreach($user as $data){
			$users 				= new User();
			$users->nip   		= $data['nip'];
			$users->name   		= $data['name'];
			$users->tgl_lahir   = $data['tgl_lahir'];
			$users->password   	= $data['password'];
			$users->email   	= $data['email'];
			$users->save();
			$role 		= Role::findByName($data['perms']);
			$users->roles()->sync([$role->id]);
		}
    }
}
