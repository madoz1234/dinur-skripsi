<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransSample extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_sample', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->integer('status')->default(0)->comment('0:Tidak Layak,1:Layak');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('trans_sample_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sample_id')->unsigned();
            $table->integer('kriteria_id')->unsigned();
            $table->integer('detail_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('sample_id')->references('id')->on('trans_sample');
            $table->foreign('kriteria_id')->references('id')->on('ref_kriteria');
            $table->foreign('detail_id')->references('id')->on('ref_kriteria_detail');
        });

        Schema::create('trans_kasus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->integer('status')->default(0)->comment('0:Tidak Layak,1:Layak');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('trans_kasus_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kasus_id')->unsigned();
            $table->integer('kriteria_id')->unsigned();
            $table->integer('detail_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('kasus_id')->references('id')->on('trans_kasus');
            $table->foreign('kriteria_id')->references('id')->on('ref_kriteria');
            $table->foreign('detail_id')->references('id')->on('ref_kriteria_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_kasus_detail');
        Schema::dropIfExists('trans_sample_detail');
        Schema::dropIfExists('trans_kasus');
        Schema::dropIfExists('trans_sample');
    }
}
