<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefKedekatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_kedekatan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kriteria_id')->unsigned();
            $table->integer('detail_id_1')->unsigned();
            $table->integer('detail_id_2')->unsigned();
            $table->float('nilai');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('kriteria_id')->references('id')->on('ref_kriteria');
            $table->foreign('detail_id_1')->references('id')->on('ref_kriteria_detail');
            $table->foreign('detail_id_2')->references('id')->on('ref_kriteria_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_kedekatan');
    }
}
