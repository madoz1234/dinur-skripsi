<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\KasusDetail;
use App\Models\Master\Kriteria;
use App\Models\Master\KriteriaDetail;

class KasusDetail extends Model
{
    /* default */
    protected $table 		= 'trans_kasus_detail';
    protected $fillable 	= ['kasus_id','kriteria_id','detail_id'];

    /* data ke log */
    // protected $log_table    = 'log_ref_kriteria';
    // protected $log_table_fk = 'ref_id';
    /* relation */
    public function kriteria(){
        return $this->belongsTo(Kriteria::class, 'kriteria_id' , 'id');
    }

    public function detail_kriteria(){
        return $this->belongsTo(KriteriaDetail::class, 'detail_id' , 'id');
    }

    /* mutator */

    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
