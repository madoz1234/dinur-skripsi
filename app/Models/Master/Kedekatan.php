<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Kriteria;
use App\Models\Master\KriteriaDetail;

class Kedekatan extends Model
{
    /* default */
    protected $table 		= 'ref_kedekatan';
    protected $fillable 	= ['kriteria_id','detail_id_1','detail_id_2','nilai'];

    /* data ke log */
    // protected $log_table    = 'log_ref_kriteria';
    // protected $log_table_fk = 'ref_id';
    /* relation */
    public function kriteria(){
        return $this->belongsTo(Kriteria::class, 'kriteria_id' , 'id');
    }

    public function kriteria_detail_1(){
        return $this->belongsTo(KriteriaDetail::class, 'detail_id_1' , 'id');
    }

    public function kriteria_detail_2(){
        return $this->belongsTo(KriteriaDetail::class, 'detail_id_2' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
