<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\KriteriaDetail;

class Kriteria extends Model
{
    /* default */
    protected $table 		= 'ref_kriteria';
    protected $fillable 	= ['nama'];

    /* data ke log */
    // protected $log_table    = 'log_ref_kriteria';
    // protected $log_table_fk = 'ref_id';
    /* relation */
    public function detail(){
        return $this->hasMany(KriteriaDetail::class, 'kriteria_id' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
