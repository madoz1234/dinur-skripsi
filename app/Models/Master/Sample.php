<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\SampleDetail;

class Sample extends Model
{
    /* default */
    protected $table 		= 'trans_sample';
    protected $fillable 	= ['nama','status'];

    /* data ke log */
    // protected $log_table    = 'log_ref_kriteria';
    // protected $log_table_fk = 'ref_id';
    /* relation */
    public function detail(){
        return $this->hasMany(SampleDetail::class, 'sample_id' , 'id');
    }
    /* mutator */

    public function saveDetail($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                $data = new SampleDetail;
                $data->kriteria_id = $key;
                $data->detail_id = $value['sample'];
                $this->detail()->save($data);
            }
        }
    }

    public function saveDetails($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                $data = new SampleDetail;
                $data->kriteria_id = $value->kriteria_id;
                $data->detail_id = $value->detail_id;
                $this->detail()->save($data);
            }
        }
    }

    public function updateDetail($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                $data = SampleDetail::find($value['id']);
                $data->kriteria_id = $key;
                $data->detail_id = $value['sample'];
                $this->detail()->save($data);
            }
        }
    }

    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
