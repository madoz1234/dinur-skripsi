<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Kriteria;

class KriteriaDetail extends Model
{
    /* default */
    protected $table 		= 'ref_kriteria_detail';
    protected $fillable 	= ['kriteria_id','nama'];

    /* data ke log */
    // protected $log_table    = 'log_ref_kriteria';
    // protected $log_table_fk = 'ref_id';
    /* relation */
    public function kriteria(){
        return $this->belongsTo(Kriteria::class, 'kriteria_id' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
