<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\SampleDetail;

class SampleDetail extends Model
{
    /* default */
    protected $table 		= 'trans_sample_detail';
    protected $fillable 	= ['sample_id','kriteria_id','detail_id'];

    /* data ke log */
    // protected $log_table    = 'log_ref_kriteria';
    // protected $log_table_fk = 'ref_id';
    /* relation */
    /* mutator */

    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
