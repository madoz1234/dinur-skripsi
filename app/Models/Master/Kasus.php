<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\KasusDetail;

class Kasus extends Model
{
    /* default */
    protected $table 		= 'trans_kasus';
    protected $fillable 	= ['nama','status'];

    /* data ke log */
    // protected $log_table    = 'log_ref_kriteria';
    // protected $log_table_fk = 'ref_id';
    /* relation */
    public function detail(){
        return $this->hasMany(KasusDetail::class, 'kasus_id' , 'id');
    }
    /* mutator */

    public function saveDetail($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                $data = new KasusDetail;
                $data->kriteria_id = $key;
                $data->detail_id = $value['kasus'];
                $this->detail()->save($data);
            }
        }
    }

    public function updateDetail($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                $data = KasusDetail::find($value['id']);
                $data->kriteria_id = $key;
                $data->detail_id = $value['kasus'];
                $this->detail()->save($data);
            }
        }
    }

    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
