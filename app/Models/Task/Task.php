<?php

namespace App\Models\Task;

use App\Models\Model;
use App\Models\Auths\User;

class Task extends Model
{
    /* default */
    protected $table 		= 'trans_task';
    protected $fillable 	= ['tgl','status','tugas','keterangan','lat','lng','user_id'];

    /* data ke log */
    protected $log_table    = 'log_trans_task';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function employe(){
        return $this->belongsTo(User::class, 'user_id' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
