<?php
namespace App\Http\Middleware;

use Closure;
use Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('sideMenu', function ($menu) {
            // $menu->add('Dashboard', 'dashboard')
            // 	 ->data('perms', 'dashboard')
            //      ->data('icon', 'fa fa-tachometer')
            //      ->active('dashboard/*');
            $menu->add('Data', 'master')
                  ->data('icon', 'fa fa-book');
                 $menu->data->add('Kriteria', 'master/kriteria')
                     ->data('perms', 'master-kriteria')
                     ->active('master/kriteria/*');
                 $menu->data->add('Nilai Kedekatan', 'master/kedekatan')
                     ->data('perms', 'master-kedekatan')
                     ->active('master/kedekatan/*');
                 $menu->data->add('Sample', 'master/sample')
                     ->data('perms', 'master-sample')
                     ->active('master/sample/*');
                 $menu->data->add('Kasus', 'master/kasus')
                     ->data('perms', 'master-kasus')
                     ->active('master/kasus/*');
	        // $menu->add('Setting', 'setting')
         //         ->data('perms', 'setting')
         //         ->data('icon', 'fa fa-users')
         //         ->active('setting/*');
        });
        return $next($request);
    }
}
