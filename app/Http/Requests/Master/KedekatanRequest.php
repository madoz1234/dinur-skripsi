<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class KedekatanRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'detail.*.data.*.datas.*.nilai'            			=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'detail.*.data.*.datas.*.nilai.required'            => 'Nilai Kedekatan tidak boleh kosong',
       ];
    }
}
