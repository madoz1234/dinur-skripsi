<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class KriteriaDetailRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'detail.*.nama'            			=> 'required|distinct',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'detail.*.nama.required'            		=> 'Nama tidak boleh kosong',
        	'detail.*.nama.distinct'            	    => 'Nama tidak boleh sama',
       ];
    }
}
