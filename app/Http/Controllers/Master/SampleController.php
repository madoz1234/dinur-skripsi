<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\SampleRequest;
use App\Models\Master\Sample;
use App\Models\Master\Kriteria;
use App\Models\Master\KriteriaDetail;
use App\Models\SessionLog;

use DB;

class SampleController extends Controller
{
    protected $routes = 'master.sample';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Sample' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Sample',
                'className' => 'text-center',
                'width' => '300px',
                'sortable' => true,
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status Sample',
                'className' => 'text-center',
                'width' => '300px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Sample::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('nama', function ($record) {
                   return $record->nama;
               })
               ->addColumn('status', function ($record) {
                  if($record->status == 1){
                      return '<span class="label label-success">Layak</span>'; 
                  }else{
                      return '<span class="label label-danger">Tidak Layak</span>';
                  }
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';
                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                   ]);
                   return $buttons;
               })
               ->rawColumns(['status', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.sample.index');
    }

    public function create()
    {
        $kriteria = Kriteria::get();
        $detail = KriteriaDetail::get();
        return $this->render('modules.master.sample.create', [
          'record' => $kriteria,
          'detail' => $detail,
        ]);
    }

    public function simpan(SampleDetailRequest $request)
    {
        DB::beginTransaction();
        if(!is_null($request->exists)){
            $hapus = SampleDetail::where('sample_id', $request->id)
                                          ->whereNotIn('id', $request->exists)
                                          ->delete();
        }

        try {
            foreach ($request->detail as $key => $value) {
                if(!empty($value['id'])){
                    $sample_detail = SampleDetail::find($value['id']);
                    $sample_detail->sample_id = $request->id;
                    $sample_detail->nama = $value['nama'];
                    $sample_detail->save();
                }else{
                    $sample_detail = new SampleDetail;
                    $sample_detail->sample_id = $request->id;
                    $sample_detail->nama = $value['nama'];
                    $sample_detail->save();
                }
            }
            DB::commit();
            return response([
              'status' => true
            ]); 
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function store(Request $request)
    {
      DB::beginTransaction();
      try {
        if($request['status']){
          $status = 1;
        }else{
          $status = 0;
        }

        $record = new Sample;
        $record->nama = $request->nama;
        $record->status = $status;
        $record->save();
        $record->saveDetail($request->detail);

        DB::commit();
          return response([
            'status' => true
          ]); 
      }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function edit($id)
    {
        $kriteria = Kriteria::get();
        $detail = KriteriaDetail::get();
        $sample = Sample::find($id);
        return $this->render('modules.master.sample.edit', [
          'sample' => $sample,
          'record' => $kriteria,
          'detail' => $detail,
        ]);
    }

    public function update(Request $request, Sample $sample)
    {
      DB::beginTransaction();
        try {
            if($request['status']){
              $status = 1;
            }else{
              $status = 0;
            }

            $record = Sample::find($request->id);
            $record->nama = $request->nama;
            $record->status = $status;
            $record->save();
            $record->updateDetail($request->detail);

        DB::commit();
          return response([
            'status' => true
          ]); 
      }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Sample $sample)
    {
        if($sample){
        
      }else{
        return response([
                'status' => true,
            ],500);
      }
    }
}
