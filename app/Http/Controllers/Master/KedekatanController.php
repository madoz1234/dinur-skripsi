<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\KedekatanRequest;
use App\Models\Master\Kriteria;
use App\Models\Master\KriteriaDetail;
use App\Models\Master\Kedekatan;
use App\Models\SessionLog;

use DB;

class KedekatanController extends Controller
{
    protected $routes = 'master.kedekatan';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Kriteria' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Kriteria',
                'className' => 'text-center',
                'width' => '300px',
                'sortable' => true,
            ],
            [
                'data' => 'bobot',
                'name' => 'bobot',
                'label' => 'Bobot',
                'className' => 'text-center',
                'width' => '300px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Kriteria::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('nama', function ($record) {
                   return $record->nama;
               })
               ->addColumn('bobot', function ($record) {
                   return $record->bobot;
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';
                   $buttons .= $this->makeButtons([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-edit text-primary"></i>',
                        'tooltip'   => 'Ubah',
                        'class'     => 'detil button',
                        'id'        => $record->id,
                   ]);
                   return $buttons;
               })
               ->rawColumns(['status', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.kedekatan.index');
    }

    public function detail(Kriteria $id)
    {
       $kedekatan = Kedekatan::get();
       return $this->render('modules.master.kedekatan.detail', [
        'record' => $id,
        'kedekatan' => $kedekatan,
       ]);
    }

    public function simpan(KedekatanRequest $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->detail as $keys => $value) {
                foreach ($value['data'] as $a => $val) {
                    foreach ($val['datas'] as $b => $data) {
                        if(!empty($data['id'])){
                            $kedekatan_detail = Kedekatan::find($data['id']);
                            $kedekatan_detail->kriteria_id = $request->id;
                            $kedekatan_detail->detail_id_1 = $a;
                            $kedekatan_detail->detail_id_2 = $b;
                            $kedekatan_detail->nilai = $data['nilai'];
                            $kedekatan_detail->save();
                        }else{
                            $kedekatan_detail = new Kedekatan;
                            $kedekatan_detail->kriteria_id = $request->id;
                            $kedekatan_detail->detail_id_1 = $a;
                            $kedekatan_detail->detail_id_2 = $b;
                            $kedekatan_detail->nilai = $data['nilai'];
                            $kedekatan_detail->save();
                        }
                    }
                }
            }
            DB::commit();
            return response([
              'status' => true
            ]); 
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }
}
