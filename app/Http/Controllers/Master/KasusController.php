<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\KasusRequest;
use App\Models\Master\Kasus;
use App\Models\Master\Kriteria;
use App\Models\Master\Sample;
use App\Models\Master\KriteriaDetail;
use App\Models\Master\KasusDetail;
use App\Models\SessionLog;

use DB;

class KasusController extends Controller
{
    protected $routes = 'master.kasus';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Kasus' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Kasus',
                'className' => 'text-center',
                'width' => '300px',
                'sortable' => true,
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status Kasus',
                'className' => 'text-center',
                'width' => '300px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Kasus::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('nama', function ($record) {
                   return $record->nama;
               })
               ->addColumn('status', function ($record) {
                  if($record->status == 1){
                      return '<span class="label label-success">Layak</span>'; 
                  }elseif($record->status == 2){
                      return '<span class="label label-warning">Proses</span>';
                  }else{
                      return '<span class="label label-danger">Tidak Layak</span>';
                  }
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';
                   if($record->status !== 2){
                     $buttons .= $this->makeButtons([
                          'type'      => 'edit',
                          'label'     => '<i class="fa fa-users text-primary"></i>',
                          'tooltip'   => 'Jadikan Sampel',
                          'class'     => 'sampel button',
                          'id'        => $record->id,
                     ]); 
                     $buttons .= '&nbsp;';
                     $buttons .= '&nbsp;';
                   }
                   $buttons .= $this->makeButtons([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-refresh text-primary"></i>',
                        'tooltip'   => 'Proses',
                        'class'     => 'detil button',
                        'id'        => $record->id,
                   ]); 
                   $buttons .= '&nbsp;';
                   $buttons .= '&nbsp;';
                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                   ]);
                   return $buttons;
               })
               ->rawColumns(['status', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.kasus.index');
    }

    public function create()
    {
        $kriteria = Kriteria::get();
        $detail = KriteriaDetail::get();
        return $this->render('modules.master.kasus.create', [
          'record' => $kriteria,
          'detail' => $detail,
        ]);
    }

    public function simpan(KasusDetailRequest $request)
    {
        DB::beginTransaction();
        if(!is_null($request->exists)){
            $hapus = KasusDetail::where('kasus_id', $request->id)
                                          ->whereNotIn('id', $request->exists)
                                          ->delete();
        }

        try {
            foreach ($request->detail as $key => $value) {
                if(!empty($value['id'])){
                    $kasus_detail = KasusDetail::find($value['id']);
                    $kasus_detail->kasus_id = $request->id;
                    $kasus_detail->nama = $value['nama'];
                    $kasus_detail->save();
                }else{
                    $kasus_detail = new KasusDetail;
                    $kasus_detail->kasus_id = $request->id;
                    $kasus_detail->nama = $value['nama'];
                    $kasus_detail->save();
                }
            }
            DB::commit();
            return response([
              'status' => true
            ]); 
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function store(Request $request)
    {
      DB::beginTransaction();
      try {
        $record = new Kasus;
        $record->nama = $request->nama;
        $record->status = 2;
        $record->save();
        $record->saveDetail($request->detail);

        DB::commit();
          return response([
            'status' => true
          ]); 
      }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function detail(Kasus $id)
    {
       $sample = Sample::get();
       $kriteria = Kriteria::get();
       return $this->render('modules.master.kasus.detail', [
        'record' => $id,
        'sample' => $sample,
        'kriteria' => $kriteria,
       ]);
    }

    public function status(Kasus $id)
    { 
      DB::beginTransaction();
      try {
        $record = new Sample;
        $record->nama = $id->nama;
        $record->status = $id->status;
        $record->save();
        $record->saveDetails($id->detail);

        $cek = Kasus::find($id->id);
        $cek->detail()->delete();
        $cek->delete();
        DB::commit();
          return response([
            'status' => true
          ]); 
      }catch (\Exception $e) {
        DB::rollback();
        return response([
          'status' => 'error',
          'message' => 'An error occurred!',
          'error' => $e->getMessage(),
        ], 500);
      }
    }

    public function edit($id)
    {
        $kriteria = Kriteria::get();
        $detail = KriteriaDetail::get();
        $kasus = Kasus::find($id);
        return $this->render('modules.master.kasus.edit', [
          'kasus' => $kasus,
          'record' => $kriteria,
          'detail' => $detail,
        ]);
    }

    public function update(Request $request, Kasus $kasus)
    {
      DB::beginTransaction();
        try {
            $record = Kasus::find($request->id);
            $record->nama = $request->nama;
            $record->status = 2;
            $record->save();
            $record->updateDetail($request->detail);

        DB::commit();
          return response([
            'status' => true
          ]); 
      }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Kasus $kasus)
    {
        if($kasus){
        
      }else{
        return response([
                'status' => true,
            ],500);
      }
    }
}
