<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\KriteriaRequest;
use App\Http\Requests\Master\KriteriaDetailRequest;
use App\Models\Master\Kriteria;
use App\Models\Master\KriteriaDetail;
use App\Models\SessionLog;

use DB;

class KriteriaController extends Controller
{
    protected $routes = 'master.kriteria';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Kriteria' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Kriteria',
                'className' => 'text-center',
                'width' => '300px',
                'sortable' => true,
            ],
            [
                'data' => 'bobot',
                'name' => 'bobot',
                'label' => 'Bobot',
                'className' => 'text-center',
                'width' => '300px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Kriteria::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('nama', function ($record) {
                   return $record->nama;
               })
               ->addColumn('bobot', function ($record) {
                   return $record->bobot;
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';
                   $buttons .= $this->makeButtons([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-eye text-primary"></i>',
                        'tooltip'   => 'Detail',
                        'class'     => 'detil button',
                        'id'        => $record->id,
                   ]);
                   $buttons .= '&nbsp;';
                   $buttons .= '&nbsp;';
                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                   ]);
                   return $buttons;
               })
               ->rawColumns(['status', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.kriteria.index');
    }

    public function create()
    {
        return $this->render('modules.master.kriteria.create');
    }

    public function detail(Kriteria $id)
    {
       return $this->render('modules.master.kriteria.detail', ['record' => $id]);
    }

    public function simpan(KriteriaDetailRequest $request)
    {
        DB::beginTransaction();
        if(!is_null($request->exists)){
            $hapus = KriteriaDetail::where('kriteria_id', $request->id)
                                          ->whereNotIn('id', $request->exists)
                                          ->delete();
        }

        try {
            foreach ($request->detail as $key => $value) {
                if(!empty($value['id'])){
                    $kriteria_detail = KriteriaDetail::find($value['id']);
                    $kriteria_detail->kriteria_id = $request->id;
                    $kriteria_detail->nama = $value['nama'];
                    $kriteria_detail->save();
                }else{
                    $kriteria_detail = new KriteriaDetail;
                    $kriteria_detail->kriteria_id = $request->id;
                    $kriteria_detail->nama = $value['nama'];
                    $kriteria_detail->save();
                }
            }
            DB::commit();
            return response([
              'status' => true
            ]); 
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function store(KriteriaRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new Kriteria;
			$record->nama = $request->nama;
            $record->bobot = $request->bobot;
	        $record->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function edit($id)
    {
        $kriteria = Kriteria::find($id);
        return $this->render('modules.master.kriteria.edit', ['record' => $kriteria]);
    }

    public function update(KriteriaRequest $request, Kriteria $kriteria)
    {
    	DB::beginTransaction();
        try {
        	$record = Kriteria::find($request->id);
			$record->nama = $request->nama;
            $record->bobot = $request->bobot;
	        $record->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Kriteria $kriteria)
    {
        if($kriteria){
    		
    	}else{
    		return response([
                'status' => true,
            ],500);
    	}
    }
}
