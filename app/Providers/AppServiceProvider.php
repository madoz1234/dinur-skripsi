<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        config(['app.locale' => 'id']);
		Carbon::setLocale('id');
		date_default_timezone_set('Asia/Jakarta');

		Relation::morphMap([
              //Providers
              'surat-penugasan-audit' 	=> 'App\Models\PersiapanAudit\PenugasanAudit\PenugasanAudit',
              'tinjauan-dokumen' 		=> 'App\Models\PersiapanAudit\TinjauanDokumen\TinjauanDokumen',
              'dokumen-detail' 			=> 'App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumenDetail',
              'rapat-internal' 			=> 'App\Models\Rapat\RapatInternal',
              'opening-meeting' 		=> 'App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting',
              'closing-meeting' 		=> 'App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting',
              'kka' 		=> 'App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetail',
              'lha' 					=> 'App\Models\KegiatanAudit\Pelaporan\LHA',
        ]);
    }
}
