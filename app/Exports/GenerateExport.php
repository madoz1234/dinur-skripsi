<?php
namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromView;

class GenerateExport implements FromView, WithTitle
{
	protected $view;
	protected $data;

	public function __construct($view,$data)
	{
		$this->view = $view;
		$this->data = $data;
	}

    public function view(): View
    {

        $view = 'Core::layouts.default_export.blade.php';
        $view = 'Core::layouts.default_export-pdf.blade.php';
    	$data = [];
    	if($this->view && $this->data){
    		$view = $this->view;
    		$data = $this->data;
    	}
        return view($view, [
            'record' => $data
        ]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        $title = isset($this->data['title'])?$this->data['title']:'Default';
        return $title;
    }
}